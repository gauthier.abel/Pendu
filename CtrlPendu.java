package game;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class CtrlPendu{
    @FXML
    Label guessedWord;
    @FXML
    Label errors;
    @FXML
    Label title;
    @FXML
    Label status;
    @FXML
    private ImageView hangman11;
    @FXML
    private ImageView hangman10;
    @FXML
    private ImageView hangman09;
    @FXML
    private ImageView hangman08;
    @FXML
    private ImageView hangman07;
    @FXML
    private ImageView hangman06;
    @FXML
    private ImageView hangman05;
    @FXML
    private ImageView hangman04;
    @FXML
    private ImageView hangman03;
    @FXML
    private ImageView hangman02;
    @FXML
    private ImageView hangman01;
    @FXML
    private ImageView hangman00;
    @FXML
    private Button giveUp;
    @FXML
    private TextField newWord;
    @FXML
    private Button newGame;

    String word;
    String guessedLetters;
    int hangmanState;
    int uniqueLetters;
    boolean end;
    @FXML
    public void initialize() {
    	boolean present;
    	char[] letters;

    	end = false;
        createEvents();
        guessedWord.setStyle("-fx-border-color:black; -fx-background-color: white;");

        hangmanState = 0;
        word = newWord.getText().toUpperCase();
        System.out.println("[" + word + "]");

        guessedWord.setText("");
        newWord.setText("");
        errors.setText("");
        status.setText("");
        status.setStyle("");

        uniqueLetters = 0;
        guessedLetters = "";

        letters = new char[word.length()];
        for(int i = 0; i < word.length(); i++){
        	present = false;
        	for(int j = 0; j < letters.length; j++){
        		if(word.charAt(i) == letters[j]) present = true;
        	}
        	if(!present){
        		uniqueLetters++;
        		letters[i] = word.charAt(i);
        	}

        	guessedWord.setText(guessedWord.getText() + "_ ");
        }

        System.out.println(uniqueLetters+ "\n");

        hangman00.setVisible(false);
        hangman01.setVisible(false);
        hangman02.setVisible(false);
        hangman03.setVisible(false);
        hangman04.setVisible(false);
        hangman05.setVisible(false);
        hangman06.setVisible(false);
        hangman07.setVisible(false);
        hangman08.setVisible(false);
        hangman09.setVisible(false);
        hangman10.setVisible(false);
        hangman11.setVisible(false);

        hangman00.setFocusTraversable(false);
        hangman01.setFocusTraversable(false);
        hangman02.setFocusTraversable(false);
        hangman03.setFocusTraversable(false);
        hangman04.setFocusTraversable(false);
        hangman05.setFocusTraversable(false);
        hangman06.setFocusTraversable(false);
        hangman07.setFocusTraversable(false);
        hangman08.setFocusTraversable(false);
        hangman09.setFocusTraversable(false);
        hangman10.setFocusTraversable(false);
        hangman11.setFocusTraversable(false);
        newGame.setFocusTraversable(false);
        newWord.setFocusTraversable(false);
        giveUp.setFocusTraversable(false);
        title.setFocusTraversable(true);
        title.requestFocus();
    }

    private void createEvents(){
    	newGame.setOnAction(x->{
    		if(newWord.getText().length() != 0) initialize();
    	});

    	title.setOnKeyReleased(new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent event) {
				if(!end && guessedWord.getText().length() != 0) checkLetter(event.getCode());
			}
    	});

    	newWord.setOnKeyReleased(new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent event) {
				if(event.getCode() == KeyCode.ENTER) newGame.fire();
			}
    	});

    	giveUp.setOnAction(x->{
			while(!end && hangmanState < 11) incrHangman();
    	});
    }


	private void checkLetter(KeyCode keyCode) {
		boolean failed = true;
		if((keyCode.impl_getCode() >= 'A' && keyCode.impl_getCode() <= 'Z') || (keyCode.impl_getCode() >= 'a' && keyCode.impl_getCode() <= 'z')){
			for(int i = 0; i < word.length(); i++){
				if((char)keyCode.impl_getCode() == word.charAt(i)){
					StringBuffer newGuessedWord = new StringBuffer();
					newGuessedWord.append(guessedWord.getText());
					newGuessedWord.setCharAt(i*2, word.charAt(i));
					guessedWord.setText(newGuessedWord.toString());

					failed = false;
				}
			}

			if(failed){
				displayError(keyCode);
			}
			else{
				displayLetter(keyCode);
			}

			System.out.println(guessedLetters.length() + " / " + uniqueLetters);
		}
	}

	private void incrHangman(){
		switch(hangmanState){
			case 0:{
		        hangman01.setVisible(true);
		        break;
			}
			case 1:{
		        hangman02.setVisible(true);
		        break;
			}
			case 2:{
		        hangman03.setVisible(true);
		        break;
			}
			case 3:{
		        hangman04.setVisible(true);
		        break;
			}
			case 4:{
		        hangman05.setVisible(true);
		        break;
			}
			case 5:{
		        hangman06.setVisible(true);
		        break;
			}
			case 6:{
		        hangman07.setVisible(true);
		        break;
			}
			case 7:{
		        hangman08.setVisible(true);
		        break;
			}
			case 8:{
		        hangman09.setVisible(true);
		        break;
			}
			case 9:{
		        hangman10.setVisible(true);
		        break;
			}
			case 10:{
		        hangman11.setVisible(true);
		        displayResult(false);
				displayWord();
		        break;
			}
			default:{
				displayResult(false);
				break;
			}
		}
		if(hangmanState < 11) hangmanState++;
	}

	private void displayLetter(KeyCode keyCode){
		boolean present = false;

		for(int i = 0; i < guessedLetters.length(); i++){
			if ((char)keyCode.impl_getCode() == guessedLetters.charAt(i)) present = true;
		}
		if(!present){
			System.out.println("Right");
			guessedLetters = guessedLetters + (char)keyCode.impl_getCode();

			if(guessedLetters.length() == uniqueLetters){
				displayResult(true);
			}
		}
	}

	private void displayError(KeyCode keyCode){
		boolean present = false;

		for(int i = 0; i < errors.getText().length(); i++){
			if ((char)keyCode.impl_getCode() == errors.getText().charAt(i)) present = true;
		}
		if(!present){
			System.out.println("Wrong");

			incrHangman();
			errors.setText(errors.getText() + " " + (char)keyCode.impl_getCode());
		}
	}

	public void displayResult(boolean win){
		if(win)status.setText("Gagn�!");
		else status.setText("Perdu...");
		end = true;
	}

	public void displayWord(){
		guessedWord.setText("");
		for(int i = 0; i < word.length(); i++){
			guessedWord.setText(guessedWord.getText() + word.charAt(i) + " ");
		}
	}
}